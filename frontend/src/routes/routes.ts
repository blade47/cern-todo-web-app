import { HOST_API } from "src/config-global";

export const routes = {
  categories: `${HOST_API}/categories`,
  tasks: `${HOST_API}/tasks`,
  task(id: number) {
    return `${HOST_API}/tasks/${id}`;
  },
  category(id: number) {
    return `${HOST_API}/categories/${id}`;
  },
  createTask(categoryId: number) {
    return `${HOST_API}/categories/${categoryId}/tasks`;
  },
};

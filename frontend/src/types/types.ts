export type Category = {
  id: number;
  name: string;
  description?: string;
  tasks: Task[];
};

export type Task = {
  id: number;
  name: string;
  description?: string;
  deadline: string;
  categoryId: number;
};

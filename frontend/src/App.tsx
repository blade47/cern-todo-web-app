import { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import UpsertCategoryDialog from "src/dialogs/upsert-category-dialog";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import CssBaseline from "@mui/material/CssBaseline";
import { Category } from "src/types/types";
import axios from "axios";
import { routes } from "src/routes/routes";
import CategoryElement from "src/components/Category";

function App() {
  const [upsertCategoryDialogOpen, setUpsertCategoryDialogOpen] =
    useState(false);

  const openUpsertCategoryDialog = () => {
    setUpsertCategoryDialogOpen(true);
  };

  const closeUpsertCategoryDialog = () => {
    setUpsertCategoryDialogOpen(false);
  };

  const [categories, setCategories] = useState<Category[]>([]);

  const onSuccess = async () => {
    await refetchCategories();
    closeUpsertCategoryDialog();
  };

  const refetchCategories = async () => {
    try {
      const { data } = await axios.get<Category[]>(routes.categories, {
        headers: {
          Accept: "application/json",
        },
      });
      setCategories(data);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.log("error message: ", error.message);
        return error.message;
      } else {
        console.log("unexpected error: ", error);
        return "An unexpected error occurred";
      }
    }
  };

  useEffect(() => {
    refetchCategories();
  }, []);

  return (
    <div className="app">
      <header className="App-header">
        <CssBaseline />
        <AppBar component="nav" color="inherit" position="sticky">
          <Toolbar variant="dense">
            <Typography variant="h5" color="primary">
              My Tasks
            </Typography>
          </Toolbar>
        </AppBar>
        <Container sx={{ p: 2 }}>
          <Stack>
            <Grid container justifyContent="flex-end">
              <Button
                variant="outlined"
                onClick={(_) => {
                  openUpsertCategoryDialog();
                }}
                sx={{ m: 4 }}
              >
                Create Category
              </Button>
            </Grid>
            <Grid container spacing={5} mt={1} p={2}>
              {categories.map((category) => (
                <CategoryElement
                  key={category.id}
                  category={category}
                  refetch={refetchCategories}
                  categories={categories}
                />
              ))}
            </Grid>
          </Stack>
          <UpsertCategoryDialog
            open={upsertCategoryDialogOpen}
            onClose={closeUpsertCategoryDialog}
            onSuccess={onSuccess}
            category={undefined}
          />
        </Container>
      </header>
    </div>
  );
}

export default App;

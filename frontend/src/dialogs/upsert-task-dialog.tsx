import { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import axios from "axios";
import { routes } from "src/routes/routes";
import { Category, Task } from "src/types/types";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import Stack from "@mui/material/Stack";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

type Props = {
  open: boolean;
  onClose: () => void;
  onSuccess: () => void;
  categoryId?: number;
  categories: Category[];
  task?: Task;
};

export default function UpsertTaskDialog({
  open,
  onClose,
  onSuccess,
  categoryId,
  categories,
  task,
}: Props) {
  const [selectedDeadline, setSelectedDeadline] =
    useState<dayjs.Dayjs>(dayjs());
  const [selectedCategory, setSelectedCategory] = useState<number | null>(null);

  useEffect(() => {
    setSelectedCategory(
      task ? task.categoryId : categoryId ?? categories[0].id,
    );
    setSelectedDeadline(task ? dayjs(task.deadline) : dayjs());
  }, [categories, categoryId, task]);

  const createTask = async (name: string, description: string) => {
    if (categoryId) {
      const { data } = await axios.post<Task>(
        routes.createTask(categoryId),
        {
          name: name,
          description: description,
          deadline: selectedDeadline.toISOString(),
          categoryId: categoryId,
        },
        {
          headers: {
            Accept: "application/json",
          },
        },
      );
      return data;
    }
    return null;
  };

  const updateTask = async (name: string, description: string) => {
    if (task) {
      const { data } = await axios.patch<Task>(
        routes.task(task.id),
        {
          name: name,
          description: description,
          deadline: selectedDeadline.toISOString(),
          categoryId: selectedCategory,
        },
        {
          headers: {
            Accept: "application/json",
          },
        },
      );
      return data;
    }
    return null;
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      fullWidth={true}
      maxWidth={"md"}
      PaperProps={{
        component: "form",
        onSubmit: async (event: React.FormEvent<HTMLFormElement>) => {
          event.preventDefault();
          const formData = new FormData(event.currentTarget);
          const formJson = Object.fromEntries((formData as any).entries());
          const name = formJson.name;
          const description = formJson.description;
          try {
            task
              ? await updateTask(name, description)
              : await createTask(name, description);
            onSuccess();
          } catch (error) {
            if (axios.isAxiosError(error)) {
              console.log("error message: ", error.message);
              return error.message;
            } else {
              console.log("unexpected error: ", error);
              return "An unexpected error occurred";
            }
          }
        },
      }}
    >
      <DialogTitle>{task ? "Update" : "Create"} Task</DialogTitle>
      <DialogContent>
        <Stack spacing={2} mt={2}>
          <TextField
            required
            margin="dense"
            id="name"
            name="name"
            defaultValue={task?.name}
            label="Name"
            fullWidth
            variant="outlined"
          />
          <TextField
            margin="dense"
            id="description"
            name="description"
            defaultValue={task?.description}
            label="Description"
            fullWidth
            variant="outlined"
            multiline
            minRows={4}
          />
          <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="en">
            <DatePicker
              disablePast
              defaultValue={task?.deadline ? dayjs(task.deadline) : dayjs()}
              onChange={(e) => {
                if (e) setSelectedDeadline(e);
              }}
            />
          </LocalizationProvider>
          <FormControl fullWidth>
            <InputLabel id="categoryIdLabel">Category</InputLabel>
            <Select
              labelId="categoryIdLabel"
              id="categoryId"
              value={selectedCategory}
              label="Category"
              onChange={(e) => setSelectedCategory(e.target.value as number)}
            >
              {categories.map((category) => (
                <MenuItem key={category.id} value={category.id}>
                  {category.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button onClick={onSuccess}>Cancel</Button>
        <Button type="submit">{task ? "Update" : "Create"} task</Button>
      </DialogActions>
    </Dialog>
  );
}

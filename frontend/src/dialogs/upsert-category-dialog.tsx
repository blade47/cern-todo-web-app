import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import axios from "axios";
import { routes } from "src/routes/routes";
import { Category } from "src/types/types";
import Stack from "@mui/material/Stack";

type Props = {
  open: boolean;
  onClose: () => void;
  onSuccess: () => void;
  category?: Category;
};

export default function UpsertCategoryDialog({
  open,
  onClose,
  onSuccess,
  category,
}: Props) {
  const createCategory = async (name: string, description: string) => {
    const { data } = await axios.post<Category>(
      routes.categories,
      {
        name: name,
        description: description,
      },
      {
        headers: {
          Accept: "application/json",
        },
      },
    );
    return data;
  };

  const updateCategory = async (name: string, description: string) => {
    if (category) {
      const { data } = await axios.patch<Category>(
        routes.category(category?.id),
        {
          name: name,
          description: description,
        },
        {
          headers: {
            Accept: "application/json",
          },
        },
      );
      return data;
    }
    return null;
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      fullWidth={true}
      maxWidth={"md"}
      PaperProps={{
        component: "form",
        onSubmit: async (event: React.FormEvent<HTMLFormElement>) => {
          event.preventDefault();
          const formData = new FormData(event.currentTarget);
          const formJson = Object.fromEntries((formData as any).entries());
          const name = formJson.name;
          const description = formJson.description;
          try {
            category
              ? await updateCategory(name, description)
              : await createCategory(name, description);
            onSuccess();
          } catch (error) {
            if (axios.isAxiosError(error)) {
              console.log("error message: ", error.message);
              return error.message;
            } else {
              console.log("unexpected error: ", error);
              return "An unexpected error occurred";
            }
          }
        },
      }}
    >
      <DialogTitle>{category ? "Update" : "Create"} Category</DialogTitle>
      <DialogContent>
        <Stack spacing={2} mt={2}>
          <TextField
            required
            margin="dense"
            id="name"
            name="name"
            defaultValue={category?.name}
            label="Name"
            fullWidth
            variant="outlined"
          />
          <TextField
            margin="dense"
            id="description"
            name="description"
            defaultValue={category?.description}
            label="Description"
            fullWidth
            variant="outlined"
            multiline
            minRows={4}
          />
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button onClick={onSuccess}>Cancel</Button>
        <Button type="submit">{category ? "Update" : "Create"} category</Button>
      </DialogActions>
    </Dialog>
  );
}

import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import Typography from "@material-ui/core/Typography";
import { Category } from "src/types/types";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import { routes } from "src/routes/routes";
import axios from "axios";
import UpsertTaskDialog from "src/dialogs/upsert-task-dialog";
import UpsertCategoryDialog from "src/dialogs/upsert-category-dialog";
import TaskElement from "./Task";

type Props = {
  category: Category;
  refetch: () => Promise<string | undefined>;
  categories: Category[];
};

export default function CategoryElement({
  category,
  refetch,
  categories,
}: Props) {
  const [upsertTaskDialogOpen, setUpsertTaskDialogOpen] = useState(false);
  const [upsertCategoryDialogOpen, setUpsertCategoryDialogOpen] =
    useState(false);
  const [selectedCategory, setSelectedCategory] = useState<Category | null>(
    null,
  );

  const openUpsertTaskDialog = () => {
    setUpsertTaskDialogOpen(true);
  };

  const openUpsertCategoryDialog = () => {
    setUpsertCategoryDialogOpen(true);
  };

  const closeUpsertCategoryDialog = () => {
    setUpsertCategoryDialogOpen(false);
  };

  const closeUpsertTaskDialog = () => {
    setUpsertTaskDialogOpen(false);
  };

  const onSuccess = async () => {
    await refetch();
    closeUpsertCategoryDialog();
    closeUpsertTaskDialog();
  };

  const deleteCategory = async (id: number) => {
    try {
      await axios.delete(routes.category(id), {
        headers: {
          Accept: "application/json",
        },
      });
      onSuccess();
    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.log("error message: ", error.message);
        return error.message;
      } else {
        console.log("unexpected error: ", error);
        return "An unexpected error occurred";
      }
    }
  };

  return (
    <Grid item xs={12}>
      <Stack>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <Stack
            direction={"row"}
            alignItems={"center"}
            justifyContent={"center"}
            spacing={1}
          >
            <ModeEditIcon
              color="primary"
              fontSize="small"
              onClick={(_) => {
                setSelectedCategory(category);
                openUpsertCategoryDialog();
              }}
              sx={{ cursor: "pointer" }}
            />
            <DeleteIcon
              color="error"
              fontSize="small"
              onClick={(_) => deleteCategory(category.id)}
              sx={{ cursor: "pointer" }}
            />
            <Typography variant="h5">{category.name}</Typography>
          </Stack>
          <Button
            size="small"
            variant="outlined"
            onClick={(_) => {
              setSelectedCategory(category);
              openUpsertTaskDialog();
            }}
          >
            Add Task
          </Button>
        </Stack>
        {category.description && (
          <Typography variant="caption">{category.description}</Typography>
        )}
      </Stack>
      <Divider sx={{ mt: 1 }} />
      <Stack mt={2} ml={2} spacing={2}>
        {category.tasks.length === 0 && (
          <Grid container alignContent={"center"} justifyContent={"center"}>
            <Typography>No tasks</Typography>
          </Grid>
        )}
        {category.tasks.map((task) => (
          <TaskElement
            key={task.id}
            task={task}
            onSuccess={onSuccess}
            categories={categories}
          />
        ))}
      </Stack>
      <Divider sx={{ mt: 3 }} />
      <UpsertCategoryDialog
        open={upsertCategoryDialogOpen}
        onClose={closeUpsertCategoryDialog}
        onSuccess={onSuccess}
        category={category}
      />
      <UpsertTaskDialog
        open={upsertTaskDialogOpen}
        onClose={closeUpsertTaskDialog}
        onSuccess={onSuccess}
        categoryId={selectedCategory?.id ?? undefined}
        categories={categories}
        task={undefined}
      />
    </Grid>
  );
}

import { Category, Task } from "src/types/types";
import Stack from "@mui/material/Stack";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import DeleteIcon from "@mui/icons-material/Delete";
import Typography from "@material-ui/core/Typography";
import dayjs from "dayjs";
import Alert from "@mui/material/Alert";
import axios from "axios";
import { routes } from "src/routes/routes";
import { useState } from "react";
import UpsertTaskDialog from "src/dialogs/upsert-task-dialog";

type Props = {
  task: Task;
  onSuccess: () => Promise<void>;
  categories: Category[];
};

export default function TaskElement({ task, onSuccess, categories }: Props) {
  const [selectedTask, setSelectedTask] = useState<Task | null>(null);
  const [upsertTaskDialogOpen, setUpsertTaskDialogOpen] = useState(false);

  const onSuccessTaskUpdate = async () => {
    await onSuccess();
    closeUpsertTaskDialog();
  };

  const openUpsertTaskDialog = () => {
    setUpsertTaskDialogOpen(true);
  };

  const closeUpsertTaskDialog = () => {
    setUpsertTaskDialogOpen(false);
  };

  const deleteTask = async (id: number) => {
    try {
      await axios.delete(routes.task(id), {
        headers: {
          Accept: "application/json",
        },
      });
      onSuccess();
    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.log("error message: ", error.message);
        return error.message;
      } else {
        console.log("unexpected error: ", error);
        return "An unexpected error occurred";
      }
    }
  };

  return (
    <Stack spacing={1}>
      <Stack
        direction={"row"}
        spacing={0.5}
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Stack
          direction={"row"}
          spacing={1}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <ModeEditIcon
            color="primary"
            fontSize="small"
            onClick={(_) => {
              setSelectedTask(task);
              openUpsertTaskDialog();
            }}
            sx={{ cursor: "pointer" }}
          />
          <DeleteIcon
            color="error"
            fontSize="small"
            onClick={(_) => deleteTask(task.id)}
            sx={{ cursor: "pointer" }}
          />
          <Typography variant="h6">{task.name}</Typography>
        </Stack>
        <Typography
          variant="body2"
          color={dayjs(task.deadline).isBefore(dayjs()) ? "error" : "primary"}
        >
          {dayjs(task.deadline).format("DD/MM/YYYY")}
        </Typography>
      </Stack>
      {task.description && (
        <Alert icon={false} severity="info">
          {task.description}
        </Alert>
      )}
      <UpsertTaskDialog
        open={upsertTaskDialogOpen}
        onClose={closeUpsertTaskDialog}
        onSuccess={onSuccessTaskUpdate}
        categoryId={undefined}
        categories={categories}
        task={selectedTask ?? undefined}
      />
    </Stack>
  );
}

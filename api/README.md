## Configuration

To execute the application, run:

```shell
cd ~/todo-web-app/api
```

```shell
./gradlew build
```

```shell
java -jar build/libs/todo-web-app.jar
```

## Following APIs have been created:

> All endpoints must start with `/api`.

------------------------------------------------------------------------------------------

#### Create new category

<details>
 <summary><code>POST</code><code><b>/categories</b></code><code>Create category</code></summary>

##### Parameters

<table>
<tr>
<td> Name </td> <td> Type </td> <td> Data type </td> <td> Description </td>
</tr>
<tr>
<td> name </td>
<td> required </td>
<td> String </td>
<td> Name of the category </td>
</tr>
<tr>
<td> description </td>
<td> required </td>
<td> String </td>
<td> Description of the category </td>
</tr>
</table>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>

```json
{
  "id":"<category-id>", 
  "name":"<category-name>", 
  "description":"<category-description>", 
  "tasks":"<list-of-tasks>"
}
```

</td>
</tr>
<tr>
<td> 500 </td>
<td>

```json
{
  "message": "Internal server error"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Get category

<details>
 <summary><code>GET</code> <code><b>/categories/{categoryId}</b></code><code>Get category</code></summary>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>

```json
{
  "id":"<category-id>", 
  "name":"<category-name>", 
  "description":"<category-description>", 
  "tasks":"<list-of-tasks>"
}
```

</td>
</tr>
<tr>
<td> 404 </td>
<td>

```json
{
  "message": "Item not found"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Get categories

<details>
 <summary><code>GET</code> <code><b>/categories</b></code><code>Get all categories</code></summary>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>

```json
[
  {
      "id":"<category-id>", 
      "name":"<category-name>", 
      "description":"<category-description>", 
      "tasks":"<list-of-tasks>"
    }
]
```

</td>
</tr>
<tr>
<td> 500 </td>
<td>

```json
{
  "message": "Internal server error"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Update category

<details>
 <summary><code>PATCH</code> <code><b>/categories/{categoryId}</b></code><code>Update category</code></summary>

##### Parameters

<table>
<tr>
<td> Name </td> <td> Type </td> <td> Data type </td> <td> Description </td>
</tr>
<tr>
<td> name </td>
<td> optional </td>
<td> String </td>
<td> Name of the category </td>
</tr>
<tr>
<td> description </td>
<td> optional </td>
<td> String </td>
<td> Description of the category </td>
</tr>
</table>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>

```json
[
  {
      "id":"<category-id>", 
      "name":"<category-name>", 
      "description":"<category-description>", 
      "tasks":"<list-of-tasks>"
    }
]
```

</td>
</tr>
<tr>
<td> 500 </td>
<td>

```json
{
  "message": "Internal server error"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Delete category

<details>
 <summary><code>DELETE</code> <code><b>/categories/{categoryId}</b></code><code>Delete category</code></summary>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>
</td>
</tr>
<tr>
<td> 500 </td>
<td>

```json
{
  "message": "Internal server error"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Create task

<details>
 <summary><code>POST</code> <code><b>/categories/{categoryId}/tasks</b></code><code>Create task</code></summary>

##### Parameters

<table>
<tr>
<td> Name </td> <td> Type </td> <td> Data type </td> <td> Description </td>
</tr>
<tr>
<td> name </td>
<td> required </td>
<td> String </td>
<td> Name of the task </td>
</tr>
<tr>
<td> description </td>
<td> required </td>
<td> String </td>
<td> Description of the task </td>
</tr>
<tr>
<td> deadline </td>
<td> required </td>
<td> DateTime </td>
<td> Deadline of the task </td>
</tr>
</table>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>

```json
[
  {
      "id":"<task-id>", 
      "name":"<task-name>", 
      "description":"<task-description>", 
      "deadline":"<task-deadline>",
      "categoryId":"<category-id>"
    }
]
```

</td>
</tr>
<tr>
<td> 404 </td>
<td>

```json
{
  "message": "Item not found"
}
```

</td>
</tr>
<tr>
<td> 500 </td>
<td>

```json
{
  "message": "Internal server error"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Get task

<details>
 <summary><code>GET</code> <code><b>/tasks/{taskId}</b></code><code>Get task</code></summary>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>

```json
{
  "id":"<task-id>", 
  "name":"<task-name>", 
  "description":"<task-description>", 
  "deadline":"<task-deadline>", 
  "categoryId":"<categoryId>"
}
```

</td>
</tr>
<tr>
<td> 404 </td>
<td>

```json
{
  "message": "Item not found"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Update task

<details>
 <summary><code>PATCH</code> <code><b>/tasks/{taskId}</b></code><code>Update task</code></summary>

##### Parameters

<table>
<tr>
<td> Name </td> <td> Type </td> <td> Data type </td> <td> Description </td>
</tr>
<tr>
<td> name </td>
<td> optional </td>
<td> String </td>
<td> Name of the task </td>
</tr>
<tr>
<td> description </td>
<td> optional </td>
<td> String </td>
<td> Description of the task </td>
</tr>
<tr>
<td> deadline </td>
<td> optional </td>
<td> DateTime </td>
<td> Deadline of the task </td>
</tr>
</table>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>

```json
{
  "id":"<task-id>",
  "name":"<task-name>",
  "description":"<task-description>",
  "deadline":"<task-deadline>",
  "categoryId":"<categoryId>"
}
```

</td>
</tr>
<tr>
<td> 404 </td>
<td>

```json
{
  "message": "Item not found"
}
```

</td>
</tr>

<tr>
<td> 500 </td>
<td>

```json
{
  "message": "Internal server error"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

#### Delete task

<details>
 <summary><code>DELETE</code> <code><b>/tasks/{taskId}</b></code><code>Delete task</code></summary>

##### Responses

<table>
<tr>
<td> Status </td> <td> Response </td>
</tr>
<tr>
<td> 200 </td>
<td>
</td>
</tr>
<tr>
<td> 500 </td>
<td>

```json
{
  "message": "Internal server error"
}
```

</td>
</tr>
</table>

</details>

------------------------------------------------------------------------------------------

## Notes

During the past year I have been using mostly Kotlin and therefore I might have missed some of the Java's latest best practices. <br>
Better exception handling (and responses) could be done: kept it simple for this first iteration. <br>
Used `spotless` for code styling:

``` ./gradlew spotlessApply ```

### Run with docker

```shell
cd ~/todo-web-app
```

```dockerfile 
docker build --tag=todo-web-app:latest . 
```

```dockerfile 
docker run -p 8081:8081 todo-web-app:latest . 
```

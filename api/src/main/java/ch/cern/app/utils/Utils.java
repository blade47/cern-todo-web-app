package ch.cern.app.utils;

import ch.cern.app.models.Category;
import ch.cern.app.models.Task;
import ch.cern.app.rest.models.CategoryUpdateUploadModel;
import ch.cern.app.rest.models.TaskUpdateUploadModel;
import java.util.Optional;

public class Utils {
  public static <T> Boolean isNotNull(T obj) {
    return obj != null;
  }

  public static <T> Optional<T> toOptional(T obj) {
    return Optional.of(obj);
  }

  public static <T> T merge(Optional<T> opt, T obj) {
    return opt.orElse(obj);
  }

  public static Category merge(CategoryUpdateUploadModel updateRequest, Category category) {
    category.setName(merge(updateRequest.name(), category.getName()));
    category.setDescription(merge(updateRequest.description(), category.getDescription()));
    return category;
  }

  public static Task merge(TaskUpdateUploadModel updateRequest, Task task, Category newCategory) {
    task.setName(merge(updateRequest.name(), task.getName()));
    task.setDescription(merge(updateRequest.description(), task.getDescription()));
    task.setDeadline(merge(updateRequest.deadline(), task.getDeadline()));
    if (isNotNull(newCategory)) task.setCategory(newCategory);
    return task;
  }
}

package ch.cern.app.services;

import static ch.cern.app.rest.translators.Translators.fromUploadModel;
import static ch.cern.app.rest.translators.Translators.toResponseModel;
import static ch.cern.app.utils.Utils.merge;

import ch.cern.app.models.Category;
import ch.cern.app.repository.CategoryRepository;
import ch.cern.app.rest.models.*;
import ch.cern.app.rest.translators.Translators;
import java.util.List;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends ch.cern.app.services.Service<Category> {

  @Autowired
  private CategoryService(CategoryRepository repository) {
    super(repository);
  }

  public CategoryResponseModel create(CategoryUploadModel uploadModel) {
    return toResponseModel(this.upsert(fromUploadModel(uploadModel)));
  }

  public List<CategoryResponseModel> getAll() {
    return StreamSupport.stream(this.fetchAll().spliterator(), false)
        .map(Translators::toResponseModel)
        .toList();
  }

  public CategoryResponseModel get(Long id) {
    return toResponseModel(this.fetch(id));
  }

  public CategoryResponseModel update(Long id, CategoryUpdateUploadModel updateRequest) {
    Category category = this.fetch(id);
    return toResponseModel(this.upsert(merge(updateRequest, category)));
  }

  public void remove(Long id) {
    this.delete(id);
  }
}

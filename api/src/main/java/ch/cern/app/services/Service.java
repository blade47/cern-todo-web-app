package ch.cern.app.services;

import ch.cern.app.models.Model;
import org.springframework.data.repository.CrudRepository;

public abstract class Service<M extends Model> {
  private final CrudRepository<M, Long> repository;

  protected Service(CrudRepository<M, Long> repository) {
    this.repository = repository;
  }

  protected Iterable<M> fetchAll() {
    return repository.findAll();
  }

  protected M upsert(M model) {
    return repository.save(model);
  }

  public M fetch(Long id) {
    return repository.findById(id).orElseThrow();
  }

  protected void delete(Long id) {
    repository.deleteById(id);
  }
}

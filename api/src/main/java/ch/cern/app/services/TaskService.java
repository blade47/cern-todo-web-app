package ch.cern.app.services;

import static ch.cern.app.rest.translators.Translators.fromUploadModel;
import static ch.cern.app.rest.translators.Translators.toResponseModel;
import static ch.cern.app.utils.Utils.merge;

import ch.cern.app.models.Category;
import ch.cern.app.models.Task;
import ch.cern.app.repository.TaskRepository;
import ch.cern.app.rest.models.TaskResponseModel;
import ch.cern.app.rest.models.TaskUpdateUploadModel;
import ch.cern.app.rest.models.TaskUploadModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskService extends ch.cern.app.services.Service<Task> {

  @Autowired
  public TaskService(TaskRepository repository) {
    super(repository);
  }

  public TaskResponseModel create(Category category, TaskUploadModel uploadModel) {
    return toResponseModel(this.upsert(fromUploadModel(uploadModel, category)));
  }

  public TaskResponseModel get(Long id) {
    return toResponseModel(this.fetch(id));
  }

  public TaskResponseModel update(
      Long id, TaskUpdateUploadModel updateRequest, Category newCategory) {
    Task task = this.fetch(id);
    return toResponseModel(this.upsert(merge(updateRequest, task, newCategory)));
  }

  public void remove(Long id) {
    this.delete(id);
  }
}

package ch.cern.app.rest.models;

import java.time.LocalDateTime;
import java.util.Optional;

public record TaskUpdateUploadModel(
    Optional<String> name,
    Optional<String> description,
    Optional<LocalDateTime> deadline,
    Optional<Long> categoryId) {}

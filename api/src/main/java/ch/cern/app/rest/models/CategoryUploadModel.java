package ch.cern.app.rest.models;

import jakarta.validation.constraints.NotNull;

public record CategoryUploadModel(@NotNull String name, String description) {}

package ch.cern.app.rest.models;

import java.time.LocalDateTime;

public record TaskResponseModel(
    Long id, String name, String description, LocalDateTime deadline, Long categoryId) {}

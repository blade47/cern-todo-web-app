package ch.cern.app.rest.resources;

import ch.cern.app.models.Category;
import ch.cern.app.rest.models.*;
import ch.cern.app.services.CategoryService;
import ch.cern.app.services.TaskService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
public class CategoryResource {

  @Autowired private CategoryService categoryService;

  @Autowired private TaskService taskService;

  @GetMapping("")
  public List<CategoryResponseModel> categories() {
    return categoryService.getAll();
  }

  @GetMapping("/{categoryId}")
  public CategoryResponseModel getCategory(@PathVariable Long categoryId) {
    return categoryService.get(categoryId);
  }

  @PostMapping("")
  @ResponseStatus(code = HttpStatus.CREATED)
  public CategoryResponseModel createCategory(@Valid @RequestBody CategoryUploadModel uploadModel) {
    return categoryService.create(uploadModel);
  }

  @PatchMapping("/{categoryId}")
  @ResponseStatus(code = HttpStatus.OK)
  public CategoryResponseModel updateCategory(
      @PathVariable Long categoryId, @RequestBody CategoryUpdateUploadModel updateRequest) {
    return categoryService.update(categoryId, updateRequest);
  }

  @DeleteMapping("/{categoryId}")
  @ResponseStatus(code = HttpStatus.OK)
  public void deleteCategory(@PathVariable Long categoryId) {
    categoryService.remove(categoryId);
  }

  @PostMapping("/{categoryId}/tasks")
  @ResponseStatus(code = HttpStatus.CREATED)
  public TaskResponseModel createTask(
      @PathVariable Long categoryId, @Valid @RequestBody TaskUploadModel uploadModel) {
    Category category = categoryService.fetch(categoryId);
    return taskService.create(category, uploadModel);
  }
}

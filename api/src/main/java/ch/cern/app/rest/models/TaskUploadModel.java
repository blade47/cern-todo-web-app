package ch.cern.app.rest.models;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

public record TaskUploadModel(
    @NotNull String name, String description, @NotNull LocalDateTime deadline) {}

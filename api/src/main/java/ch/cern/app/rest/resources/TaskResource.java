package ch.cern.app.rest.resources;

import ch.cern.app.models.Category;
import ch.cern.app.rest.models.TaskResponseModel;
import ch.cern.app.rest.models.TaskUpdateUploadModel;
import ch.cern.app.services.CategoryService;
import ch.cern.app.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tasks")
public class TaskResource {

  @Autowired private TaskService taskService;

  @Autowired private CategoryService categoryService;

  @GetMapping("/{taskId}")
  @ResponseStatus(code = HttpStatus.OK)
  public TaskResponseModel getTask(@PathVariable Long taskId) {
    return taskService.get(taskId);
  }

  @PatchMapping("/{taskId}")
  @ResponseStatus(code = HttpStatus.OK)
  public TaskResponseModel updateTask(
      @PathVariable Long taskId, @RequestBody TaskUpdateUploadModel updateRequest) {
    Category category =
        updateRequest.categoryId().isPresent()
            ? categoryService.fetch(updateRequest.categoryId().get())
            : null;
    return taskService.update(taskId, updateRequest, category);
  }

  @DeleteMapping("/{taskId}")
  @ResponseStatus(code = HttpStatus.OK)
  public void deleteTask(@PathVariable Long taskId) {
    taskService.remove(taskId);
  }
}

package ch.cern.app.rest.models;

import java.util.List;

public record CategoryResponseModel(
    Long id, String name, String description, List<TaskResponseModel> tasks) {}

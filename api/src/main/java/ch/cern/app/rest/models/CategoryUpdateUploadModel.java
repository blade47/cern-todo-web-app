package ch.cern.app.rest.models;

import java.util.Optional;

public record CategoryUpdateUploadModel(Optional<String> name, Optional<String> description) {}

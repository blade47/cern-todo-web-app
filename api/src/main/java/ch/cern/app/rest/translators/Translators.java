package ch.cern.app.rest.translators;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

import ch.cern.app.models.Category;
import ch.cern.app.models.Task;
import ch.cern.app.rest.models.CategoryResponseModel;
import ch.cern.app.rest.models.CategoryUploadModel;
import ch.cern.app.rest.models.TaskResponseModel;
import ch.cern.app.rest.models.TaskUploadModel;

public final class Translators {

  public static Category fromUploadModel(CategoryUploadModel categoryUploadModel) {
    return new Category(categoryUploadModel.name(), categoryUploadModel.description());
  }

  public static CategoryResponseModel toResponseModel(Category category) {
    return new CategoryResponseModel(
        category.getId(),
        category.getName(),
        category.getDescription(),
        emptyIfNull(category.getTasks()).stream().map(Translators::toResponseModel).toList());
  }

  public static Task fromUploadModel(TaskUploadModel taskUploadModel, Category category) {
    return new Task(
        taskUploadModel.name(),
        taskUploadModel.description(),
        taskUploadModel.deadline(),
        category);
  }

  public static TaskResponseModel toResponseModel(Task task) {
    return new TaskResponseModel(
        task.getId(),
        task.getName(),
        task.getDescription(),
        task.getDeadline(),
        task.getCategory().getId());
  }
}

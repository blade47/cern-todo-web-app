package ch.cern.app.exceptions;

public record ErrorResponse(String message) {}

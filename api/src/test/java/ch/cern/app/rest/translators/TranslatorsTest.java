package ch.cern.app.rest.translators;

import static ch.cern.app.rest.translators.Translators.fromUploadModel;
import static ch.cern.app.rest.translators.Translators.toResponseModel;
import static com.google.common.truth.Truth.assertThat;

import ch.cern.app.models.Category;
import ch.cern.app.models.Task;
import ch.cern.app.rest.models.CategoryResponseModel;
import ch.cern.app.rest.models.CategoryUploadModel;
import ch.cern.app.rest.models.TaskResponseModel;
import ch.cern.app.rest.models.TaskUploadModel;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class TranslatorsTest {

  @Test
  void categoryTranslate_fromUploadModel() {
    CategoryUploadModel uploadModel = new CategoryUploadModel("test", "test");

    Category translated = fromUploadModel(uploadModel);

    assertThat(translated.getName()).isEqualTo(uploadModel.name());
    assertThat(translated.getDescription()).isEqualTo(uploadModel.description());
  }

  @Test
  void categoryTranslate_toResponseModel() {
    Category category = new Category("test", "test");

    CategoryResponseModel translated = toResponseModel(category);

    assertThat(translated.name()).isEqualTo(category.getName());
    assertThat(translated.description()).isEqualTo(category.getDescription());
  }

  @Test
  void taskTranslate_fromUploadModel() {
    Category category = new Category("category", "category");
    TaskUploadModel uploadModel = new TaskUploadModel("test", "test", LocalDateTime.now());

    Task translated = fromUploadModel(uploadModel, category);

    assertThat(translated.getName()).isEqualTo(uploadModel.name());
    assertThat(translated.getDescription()).isEqualTo(uploadModel.description());
    assertThat(translated.getDeadline()).isEqualTo(uploadModel.deadline());
    assertThat(translated.getCategory()).isEqualTo(category);
  }

  @Test
  void taskTranslate_toResponseModel() {
    Category category = new Category("category", "category");
    category.setId(1L);
    Task task = new Task("test", "test", LocalDateTime.now(), category);

    TaskResponseModel translated = toResponseModel(task);

    assertThat(translated.name()).isEqualTo(task.getName());
    assertThat(translated.description()).isEqualTo(task.getDescription());
    assertThat(translated.deadline()).isEqualTo(task.getDeadline());
    assertThat(translated.categoryId()).isEqualTo(task.getCategory().getId());
  }
}

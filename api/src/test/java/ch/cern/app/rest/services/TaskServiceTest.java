package ch.cern.app.rest.services;

import static ch.cern.app.utils.Utils.toOptional;
import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

import ch.cern.app.models.Category;
import ch.cern.app.models.Task;
import ch.cern.app.repository.TaskRepository;
import ch.cern.app.rest.models.TaskUpdateUploadModel;
import ch.cern.app.rest.models.TaskUploadModel;
import ch.cern.app.services.TaskService;
import java.time.LocalDateTime;
import net.datafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TaskServiceTest {

  @Mock private TaskRepository repository;

  @InjectMocks private TaskService service;

  private Category category;
  private Task task;

  private final Faker faker = new Faker();

  @BeforeEach
  public void setUp() {
    this.category = new Category(faker.funnyName().name(), faker.lorem().paragraph());
    category.setId(1L);
    this.task =
        new Task(
            faker.funnyName().name(), faker.lorem().paragraph(), LocalDateTime.now(), category);

    lenient().when(repository.save(any())).thenReturn(task);
    lenient().when(repository.findById(any())).thenReturn(toOptional(task));
    lenient().doNothing().when(repository).deleteById(any());
  }

  @Test
  void createTask() {
    TaskUploadModel uploadModel =
        new TaskUploadModel(task.getName(), task.getDescription(), LocalDateTime.now());

    service.create(category, uploadModel);

    verify(repository, times(1))
        .save(
            argThat(
                it -> {
                  assertThat(it.getName()).isEqualTo(uploadModel.name());
                  assertThat(it.getDescription()).isEqualTo(uploadModel.description());
                  assertThat(it.getDeadline()).isEqualTo(uploadModel.deadline());
                  assertThat(it.getCategory().getId()).isEqualTo(category.getId());
                  return true;
                }));
  }

  @Test
  void getTask() {
    Long id = 1L;
    service.get(id);

    verify(repository, times(1))
        .findById(
            argThat(
                it -> {
                  assertThat(it).isEqualTo(id);
                  return true;
                }));
  }

  @Test
  void updateTask_withNewCategory() {
    TaskUpdateUploadModel updateRequest =
        new TaskUpdateUploadModel(
            toOptional(task.getName()),
            toOptional(task.getDescription()),
            toOptional(task.getDeadline()),
            toOptional(category.getId()));

    Category newCategory = new Category(faker.dragonBall().character(), faker.lorem().paragraph());
    newCategory.setId(20L);

    service.update(task.getId(), updateRequest, newCategory);

    verify(repository, times(1))
        .findById(
            argThat(
                it -> {
                  assertThat(it).isEqualTo(task.getId());
                  return true;
                }));

    verify(repository, times(1))
        .save(
            argThat(
                it -> {
                  assertThat(it.getName()).isEqualTo(task.getName());
                  assertThat(it.getDescription()).isEqualTo(task.getDescription());
                  assertThat(it.getDeadline()).isEqualTo(task.getDeadline());
                  assertThat(it.getCategory().getId()).isEqualTo(newCategory.getId());
                  return true;
                }));
  }

  @Test
  void updateTask_withoutNewCategory() {
    TaskUpdateUploadModel updateRequest =
        new TaskUpdateUploadModel(
            toOptional(task.getName()),
            toOptional(task.getDescription()),
            toOptional(task.getDeadline()),
            null);

    service.update(task.getId(), updateRequest, null);

    verify(repository, times(1))
        .findById(
            argThat(
                it -> {
                  assertThat(it).isEqualTo(task.getId());
                  return true;
                }));

    verify(repository, times(1))
        .save(
            argThat(
                it -> {
                  assertThat(it.getName()).isEqualTo(task.getName());
                  assertThat(it.getDescription()).isEqualTo(task.getDescription());
                  assertThat(it.getDeadline()).isEqualTo(task.getDeadline());
                  assertThat(it.getCategory().getId()).isEqualTo(category.getId());
                  return true;
                }));
  }

  @Test
  void deleteTask() {
    Long id = 1L;
    service.remove(id);

    verify(repository, times(1))
        .deleteById(
            argThat(
                it -> {
                  assertThat(it).isEqualTo(id);
                  return true;
                }));
  }
}

package ch.cern.app.rest.services;

import static ch.cern.app.utils.Utils.toOptional;
import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

import ch.cern.app.models.Category;
import ch.cern.app.repository.CategoryRepository;
import ch.cern.app.rest.models.CategoryUpdateUploadModel;
import ch.cern.app.rest.models.CategoryUploadModel;
import ch.cern.app.services.CategoryService;
import java.util.List;
import net.datafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

  @Mock private CategoryRepository repository;

  @InjectMocks private CategoryService service;

  private Category category;

  private final Faker faker = new Faker();

  @BeforeEach
  public void setUp() {
    this.category = new Category(faker.funnyName().name(), faker.lorem().paragraph());
    category.setId(1L);

    lenient().when(repository.save(any())).thenReturn(category);
    lenient().when(repository.findById(any())).thenReturn(toOptional(category));
    lenient().when(repository.findAll()).thenReturn(List.of(category));
    lenient().doNothing().when(repository).deleteById(any());
  }

  @Test
  void createCategory() {
    CategoryUploadModel uploadModel =
        new CategoryUploadModel(category.getName(), category.getDescription());

    service.create(uploadModel);

    verify(repository, times(1))
        .save(
            argThat(
                it -> {
                  assertThat(it.getName()).isEqualTo(uploadModel.name());
                  assertThat(it.getDescription()).isEqualTo(uploadModel.description());
                  return true;
                }));
  }

  @Test
  void getCategories() {
    service.getAll();

    verify(repository, times(1)).findAll();
  }

  @Test
  void getCategory() {
    Long id = 1L;
    service.get(id);

    verify(repository, times(1))
        .findById(
            argThat(
                it -> {
                  assertThat(it).isEqualTo(id);
                  return true;
                }));
  }

  @Test
  void updateCategory() {
    String updatedName = faker.dragonBall().character();
    String updatedDescription = faker.lorem().paragraph();

    CategoryUpdateUploadModel updateRequest =
        new CategoryUpdateUploadModel(toOptional(updatedName), toOptional(updatedDescription));

    assertThat(updatedName).isNotEqualTo(category.getName());
    assertThat(updatedDescription).isNotEqualTo(category.getDescription());

    service.update(category.getId(), updateRequest);

    verify(repository, times(1))
        .findById(
            argThat(
                it -> {
                  assertThat(it).isEqualTo(category.getId());
                  return true;
                }));

    verify(repository, times(1))
        .save(
            argThat(
                it -> {
                  assertThat(it.getName()).isEqualTo(updatedName);
                  assertThat(it.getDescription()).isEqualTo(updatedDescription);
                  return true;
                }));
  }

  @Test
  void deleteCategory() {
    Long id = 1L;
    service.remove(id);

    verify(repository, times(1))
        .deleteById(
            argThat(
                it -> {
                  assertThat(it).isEqualTo(id);
                  return true;
                }));
  }
}

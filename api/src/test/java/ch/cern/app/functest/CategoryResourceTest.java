package ch.cern.app.functest;

import static ch.cern.app.utils.Utils.toOptional;
import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ch.cern.app.models.Category;
import ch.cern.app.models.Task;
import ch.cern.app.repository.CategoryRepository;
import ch.cern.app.repository.TaskRepository;
import ch.cern.app.rest.models.CategoryUpdateUploadModel;
import ch.cern.app.rest.models.CategoryUploadModel;
import ch.cern.app.rest.models.TaskUploadModel;
import ch.cern.app.utils.JsonUtils;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import net.datafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class CategoryResourceTest {

  @Autowired private MockMvc mvc;

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private TaskRepository taskRepository;

  private final Faker faker = new Faker();

  @AfterEach
  void setUp() {
    categoryRepository.deleteAll();
    taskRepository.deleteAll();
  }

  @Test
  void givenCategories_whenGetCategories_thenReturnJsonArray() throws Exception {
    Category category = insertRandomCategory();

    mvc.perform(get("/categories").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].name", is(category.getName())))
        .andExpect(jsonPath("$[0].description", is(category.getDescription())));
  }

  @Test
  void givenCategories_whenGetValidCategory_thenReturnCategoryJson() throws Exception {
    insertRandomCategory();
    Category category = insertRandomCategory();

    mvc.perform(
            get("/categories/{categoryId}", category.getId())
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(category.getName())))
        .andExpect(jsonPath("$.description", is(category.getDescription())));
  }

  @Test
  void whenGetInvalidCategory_thenReturn404() throws Exception {
    mvc.perform(get("/categories/{categoryId}", 1).contentType(MediaType.APPLICATION_JSON))
        .andExpect(
            result ->
                assertThat(result.getResolvedException())
                    .isInstanceOf(NoSuchElementException.class))
        .andExpect(status().isNotFound());
  }

  @Test
  void whenCreateCategory_thenReturnCreatedCategory() throws Exception {
    CategoryUploadModel content = new CategoryUploadModel("test", "test");

    mvc.perform(
            post("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(content)))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(content.name())))
        .andExpect(jsonPath("$.description", is(content.description())));
  }

  @Test
  void whenCreateCategoryWithoutDescription_thenReturnCreatedCategory() throws Exception {
    CategoryUploadModel content = new CategoryUploadModel("test", null);

    mvc.perform(
            post("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(content)))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(content.name())));
  }

  @Test
  void whenCreateCategoryWithAlreadyExistentName_thenReturn500() throws Exception {
    String categoryName = "test";
    insertRandomCategory(categoryName);
    CategoryUploadModel content = new CategoryUploadModel(categoryName, "test");

    mvc.perform(
            post("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(content)))
        .andExpect(
            result ->
                assertThat(result.getResolvedException())
                    .isInstanceOf(DataIntegrityViolationException.class))
        .andExpect(status().is5xxServerError());
  }

  @Test
  void givenCategory_whenUpdatingCategory_thenReturnUpdatedCategory() throws Exception {
    Category category = insertRandomCategory();
    CategoryUpdateUploadModel content =
        new CategoryUpdateUploadModel(toOptional("updated"), toOptional("updated"));

    mvc.perform(
            patch("/categories/{categoryId}", category.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(content)))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(content.name().get())))
        .andExpect(jsonPath("$.description", is(content.description().get())));
  }

  @Test
  void givenCategory_whenCreateTask_thenReturnCreatedTask() throws Exception {
    Category category = insertRandomCategory();
    TaskUploadModel content = new TaskUploadModel("task", "task", LocalDateTime.now());

    mvc.perform(
            post("/categories/{categoryId}/tasks", category.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(content)))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(content.name())))
        .andExpect(jsonPath("$.deadline", is(content.deadline().toString())))
        .andExpect(jsonPath("$.description", is(content.description())));
  }

  @Test
  void givenCategory_whenCreateTaskWithoutDescription_thenReturnCreatedTask() throws Exception {
    Category category = insertRandomCategory();
    TaskUploadModel content = new TaskUploadModel("task", null, LocalDateTime.now());

    mvc.perform(
            post("/categories/{categoryId}/tasks", category.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(content)))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(content.name())));
  }

  @Test
  void givenCategory_whenDeleteCategory_thenReturnEmptyResponse() throws Exception {
    Category category = insertRandomCategory();
    insertRandomTask(category);

    mvc.perform(
            delete("/categories/{categoryId}", category.getId())
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    assertThat(categoryRepository.count() == 0).isTrue();
    // Delete category associated tasks
    assertThat(taskRepository.count() == 0).isTrue();
  }

  @Test
  void givenCategoryAndAssociatedTasks_whenGetCategory_thenReturnCategoryWithAssociatedTasks()
      throws Exception {
    Category category = insertRandomCategory();
    Category category2 = insertRandomCategory();
    Task task1 = insertRandomTask(category);
    Task task2 = insertRandomTask(category);
    insertRandomTask(category2);

    mvc.perform(
            get("/categories/{categoryId}", category.getId())
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(category.getName())))
        .andExpect(jsonPath("$.description", is(category.getDescription())))
        .andExpect(jsonPath("$.tasks", hasSize(2)))
        .andExpect(jsonPath("$.tasks[0].name", is(task1.getName())))
        .andExpect(jsonPath("$.tasks[0].description", is(task1.getDescription())))
        .andExpect(jsonPath("$.tasks[1].name", is(task2.getName())))
        .andExpect(jsonPath("$.tasks[1].description", is(task2.getDescription())));
  }

  private void insertRandomCategory(String name) {
    categoryRepository.save(new Category(name, "description"));
  }

  private Category insertRandomCategory() {
    return categoryRepository.save(
        new Category(faker.dragonBall().character(), faker.chuckNorris().fact()));
  }

  private Task insertRandomTask(Category category) {
    return taskRepository.save(
        new Task(
            faker.dragonBall().character(),
            faker.chuckNorris().fact(),
            LocalDateTime.now(),
            category));
  }
}

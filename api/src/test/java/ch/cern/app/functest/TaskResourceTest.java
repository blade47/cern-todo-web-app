package ch.cern.app.functest;

import static ch.cern.app.utils.Utils.toOptional;
import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import ch.cern.app.models.Category;
import ch.cern.app.models.Task;
import ch.cern.app.repository.CategoryRepository;
import ch.cern.app.repository.TaskRepository;
import ch.cern.app.rest.models.TaskUpdateUploadModel;
import ch.cern.app.utils.JsonUtils;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.NoSuchElementException;
import net.datafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class TaskResourceTest {

  @Autowired private MockMvc mvc;

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private TaskRepository taskRepository;

  private final Faker faker = new Faker();

  @AfterEach
  void setUp() {
    categoryRepository.deleteAll();
    taskRepository.deleteAll();
  }

  @Test
  void givenTasks_whenGetTask_thenReturnTaskJson() throws Exception {
    Category category = insertRandomCategory();
    Task task = insertRandomTask(category);

    mvc.perform(get("/tasks/{taskId}", task.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(task.getName())))
        .andExpect(jsonPath("$.description", is(task.getDescription())))
        .andExpect(jsonPath("$.deadline", is(task.getDeadline() + ":00")))
        .andExpect(jsonPath("$.categoryId", is(category.getId().intValue())));
  }

  @Test
  void whenGetInvalidTask_thenReturn404() throws Exception {
    mvc.perform(get("/tasks/{taskId}", 1).contentType(MediaType.APPLICATION_JSON))
        .andExpect(
            result ->
                assertThat(result.getResolvedException())
                    .isInstanceOf(NoSuchElementException.class))
        .andExpect(status().isNotFound());
  }

  @Test
  void givenTask_whenUpdatingTask_thenReturnUpdatedTask() throws Exception {
    Category category = insertRandomCategory();
    Task task = insertRandomTask(category);
    Category newCategory = insertRandomCategory();

    TaskUpdateUploadModel content =
        new TaskUpdateUploadModel(
            toOptional("updated"),
            toOptional("updated"),
            toOptional(LocalDateTime.now().plusDays(1).truncatedTo(ChronoUnit.MINUTES)),
            toOptional(newCategory.getId()));

    mvc.perform(
            patch("/tasks/{taskId}", task.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.toJson(content)))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(content.name().get())))
        .andExpect(jsonPath("$.description", is(content.description().get())))
        .andExpect(jsonPath("$.deadline", is(content.deadline().get() + ":00")))
        .andExpect(jsonPath("$.categoryId", is(newCategory.getId().intValue())));
  }

  @Test
  void givenTask_whenDeleteTask_thenReturnEmptyResponse() throws Exception {
    Category category = insertRandomCategory();
    Task task = insertRandomTask(category);

    mvc.perform(delete("/tasks/{taskId}", task.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    assertThat(taskRepository.count() == 0).isTrue();
    // Do not delete task category
    assertThat(categoryRepository.count() == 1).isTrue();
  }

  private Category insertRandomCategory() {
    return categoryRepository.save(
        new Category(faker.dragonBall().character(), faker.chuckNorris().fact()));
  }

  private Task insertRandomTask(Category category) {
    return taskRepository.save(
        new Task(
            faker.dragonBall().character(),
            faker.chuckNorris().fact(),
            LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES),
            category));
  }
}
